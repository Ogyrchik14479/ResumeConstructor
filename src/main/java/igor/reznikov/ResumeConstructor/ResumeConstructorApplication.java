package igor.reznikov.ResumeConstructor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResumeConstructorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ResumeConstructorApplication.class, args);
	}

}
