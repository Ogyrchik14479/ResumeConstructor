package igor.reznikov.ResumeConstructor.enums;

public enum WorkScheduleEnum {
    FULL_TIME,
    SHIFT_SCHEDULE,
    FLEXIBLE_SCHEDULE,
    REMOTE_WORK,
    SHIFT_METHOD
}
