package igor.reznikov.ResumeConstructor.enums;

public enum FormOfEducationEnum {
    INTRAMURAL, //очная
    CORRESPONDENCE, //заочная
    DISTANCE, //дистанционное
    EVENING //очно-заочное(вечернее)
}
