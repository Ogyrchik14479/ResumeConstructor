package igor.reznikov.ResumeConstructor.enums;

public enum MaritalStatusEnum {
    SINGLE,
    NOT_MARRIED
}
