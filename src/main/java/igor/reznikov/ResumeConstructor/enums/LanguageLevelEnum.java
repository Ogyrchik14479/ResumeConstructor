package igor.reznikov.ResumeConstructor.enums;

public enum LanguageLevelEnum {
    BEGINNER,
    ELEMENTARY,
    PRE_INTERMEDIATE,
    INTERMEDIATE,
    INTERMEDIATE_PLUS,
    UPPER_INTERMEDIATE,
    ADVANCED,
    PROFICIENCY
}
