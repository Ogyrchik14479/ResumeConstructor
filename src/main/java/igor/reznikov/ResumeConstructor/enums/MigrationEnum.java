package igor.reznikov.ResumeConstructor.enums;

public enum MigrationEnum {
    IMPOSSIBLE,
    POSSIBLE,
    UNDESIRABLE,
    DESIRABLE
}
