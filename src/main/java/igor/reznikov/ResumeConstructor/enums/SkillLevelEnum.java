package igor.reznikov.ResumeConstructor.enums;

public enum SkillLevelEnum {
    EXPERT,
    EXPERIENCED,
    SKILFUL,
    BEGINNER,
    NOVICE
}
