package igor.reznikov.ResumeConstructor.enums;

public enum EmploymentTypeEnum {
    FULL,
    PARTIAL,
    PROJECT,
    INTERNSHIP,
    VOLUNTEERING
}
