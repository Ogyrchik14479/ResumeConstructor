package igor.reznikov.ResumeConstructor.enums;

public enum DegreeOfEducationEnum {
    AVERAGE,
    SECONDARY_INCOMPLETE,
    SECONDARY_VOCATIONAL,
    HIGHER,
    HIGHER_INCOMPLETE
}
